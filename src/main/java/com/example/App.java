package com.example;

import java.util.Arrays;

public class App {
    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.println("args not provided");
            return;
        }
        int[] numbers = new int[args.length];
        for (int i = 0; i < args.length; i++) {
            try {
                numbers[i] = Integer.parseInt(args[i]);
            } catch (NumberFormatException e) {
                System.out.println("val at index " + i + " is not an integer");
                return;
            }
        }
        Arrays.sort(numbers);
        System.out.println("sorted array is : " + Arrays.toString(numbers));
    }
}
