package com.example;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertTrue;

@RunWith(Parameterized.class)
public class AppTest {

    private String[] inputArgs;
    private Object expectedOutput;

    public AppTest(String[] inputArgs, Object expectedOutput) {
        this.inputArgs = inputArgs;
        this.expectedOutput = expectedOutput;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {new String[]{"6", "2", "1", "0", "5", "4", "3", "7", "9", "8"},
                        "sorted array is : [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]"},
                {new String[]{"5"}, "sorted array is : [5]"},
                {new String[]{}, "args not provided"},
                {new String[]{"f"}, "val at index 0 is not an integer"},
                {new String[]{"1", "h", "0"}, "val at index 1 is not an integer"},
                {new String[]{"6", "5", "4", "3", "2", "1", "0"},
                        "sorted array is : [0, 1, 2, 3, 4, 5, 6]"}
        });
    }

    @Test
    public void testApp() {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PrintStream originalOut = System.out;
        System.setOut(new PrintStream(outputStream));

        App.main(inputArgs);

        System.setOut(originalOut);

        String actualOutput = outputStream.toString().trim();
        if (expectedOutput instanceof int[]) {
            int[] expectedIntArray = (int[]) expectedOutput;
            String[] actualOutputArray = actualOutput.split("\\s*,\\s*");
            int[] actualIntArray = Arrays.stream(actualOutputArray)
                    .mapToInt(Integer::parseInt)
                    .toArray();
            assertArrayEquals(expectedIntArray, actualIntArray);
        } else if (expectedOutput instanceof String) {
            String expectedString = (String) expectedOutput;
            assertTrue(actualOutput.contains(expectedString));
        }
    }
}
